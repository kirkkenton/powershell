."C:\scripts\Functions\kirksutils.ps1"
."C:\scripts\Functions\logging.ps1"

change-title "Get AD Data Script"
<# Script Main #>

$startDate = Get-Date
$LogFile = "C:\PSLogging\Import-ADData-{0}.log" -f $startDate.ToString("yyyyMMdd-HHmm")
$DebugPreference = "Continue"
Log-Start -LogPath $LogFile -ScriptName "Import AD Data"
# CSVDE Export file and then imported back to Transform dates and userAccountControl
$file = "C:\Data\ADUserExport-{0}.csv" -f $startDate.ToString("yyyyMMdd-HHmm")
# Final output file with transformed data, ready to import to DB
$outfile = $file.substring(0,$file.length-4)+"-OUTPUT.csv"
# LDAP Search Base
$searchBase = "DC=stjoe,DC=org"
# LDAP Filter
$ldapFilter = "(&(objectCategory=Person)(objectClass=user))"
# List of AD Attributes to retrieve
$attributeList = "DN,
				sn,
				title,
				givenName,
				initials,
				whenCreated,
				department,
				company,
				userAccountControl,
				employeeID,
				employeeNumber,
				accountExpires,
				sAMAccountName,
				mail,
				manager,
				AccountType,
				sjhsEnterpriseID,
				division,
				l,
				middleName,
				lastlogontimestamp,
				userPrincipalName"

Log-Write -LogPath $Logfile -LineValue "Starting AD Export"
Try
{
	csvde -d $searchBase -r $ldapFilter -l $attributeList -f $file
}
Catch
{
	Log-Error -LogPath $LogFile -ErrorDesc "Error exporting AD Data.  EXITING." -ExitGracefully $true
}

Log-Write -LogPath $LogFile -LineValue "Importing AD Data file back into script"
$data = Import-Csv -Path $file

$counter = 0
while($counter -lt $data.Count)
{
	$of = $data.count
	Log-Write -LogPath $LogFile -LineValue "Processing $counter of $of"

	# initialize variables
	$whenCreated = "1/1/1900"
	$accountIsDisabled = ""
	[datetime]$accountExpires = "1/1/1900"
	[datetime]$lastLogonTimestamp =  "1/1/1900"

	If ($data[$counter] -ne $null)
	{
	
		#employeeid
		if ($data[$counter].employeeid -ne $null -or $data[$counter].employeeid -ne "")
		{
		if ($data[$counter].employeeid -as [int])
		{} Else	{
			$data[$counter].employeeid = ""
		}
		}
	
	# whenCreated
		if($data[$counter].whenCreated -ne $null)
		{
			Try
			{
				[Datetime]$whenCreated = convertZuluDate -arg $data[$counter].whenCreated
				if($whenCreated -ne [datetime]"1/1/1900")
				{
				$data[$counter].whenCreated = $whenCreated.ToString("M-d-yyyy H:mm:ss")
				}
			}
			Catch
			{
				Log-Error -LogPath $LogFile -ErrorDesc "ERROR: whenCreated" -ExitGracefully $false
				Log-Error -LogPath $LogFile -ErrorDesc $_.Exception.Message -ExitGracefully $false
			}
		}
		
	# userAccountControl	
		if($data[$counter].userAccountControl -ne $null)
		{
			Try
			{
				$accountIsDisabled = AccountDisabled $data[$counter].userAccountControl
				$data[$counter] | Add-Member -MemberType NoteProperty -Name AccountIsDisabled -Value $accountIsDisabled
			}
						Catch
			{
				Log-Error -LogPath $LogFile -ErrorDesc "ERROR: userAccountControl" -ExitGracefully $false
				Log-Error -LogPath $LogFile -ErrorDesc $_.Exception.Message -ExitGracefully $false
			}
		}
		
	# accountExpires
		if($data[$counter].accountExpires -ne $null)
		{		
			Try
			{
				If($data[$counter].accountExpires -eq 0 -or $data[$counter].accountExpires -eq 9223372036854775807)
				{
					$data[$counter].accountExpires = ""
				} else 
				{
					$acctExp = [datetime]::FromFileTimeutc($data[$counter].accountExpires)
					if($acctExp -ne [datetime]"1/1/1900")
					{
						$data[$counter].accountExpires = $acctExp.ToString("M-d-yyyy H:mm:ss")
					}
				}
			}
			Catch
			{
				Log-Error -LogPath $LogFile -ErrorDesc "ERROR: accountExpires" -ExitGracefully $false
				Log-Error -LogPath $LogFile -ErrorDesc $_.Exception.Message -ExitGracefully $false
			}
		}
	#lastLogonTimestamp
		if($data[$counter].lastLogonTimestamp -ne $null)
		{
			Try
			{
				$lastLogonTimestamp = [datetime]::FromFileTimeUtc($data[$counter].lastLogonTimestamp)
				if($lastLogonTimestamp -ge [datetime]"1/1/1901")
				{
					$data[$counter].lastLogonTimestamp = $lastLogonTimestamp.ToString("M-d-yyyy H:mm:ss")
				}
			}
			Catch
			{
				Log-Error -LogPath $LogFile -ErrorDesc "ERROR: lastLogonTimestamp" -ExitGracefully $false
				Log-Error -LogPath $LogFile -ErrorDesc $_.Exception.Message -ExitGracefully $false
			}
		}
		# pwdLastSet
		if($data[$counter].pwdLastSet -ne $null)
		{		
			Try
			{
				If($data[$counter].pwdLastSet -eq 0 -or $data[$counter].pwdLastSet -eq 9223372036854775807)
				{
					$data[$counter].pwdLastSet = ""
				} else 
				{
					$pwdLastSet = [datetime]::FromFileTimeutc($data[$counter].pwdLastSet)
					if($pwdLastSet -ne [datetime]"1/1/1900")
					{
						$data[$counter].pwdLastSet = $pwdLastSet.ToString("M-d-yyyy H:mm:ss")
					}
				}
			}
			Catch
			{
				Log-Error -LogPath $LogFile -ErrorDesc "ERROR: accountExpires" -ExitGracefully $false
				Log-Error -LogPath $LogFile -ErrorDesc $_.Exception.Message -ExitGracefully $false
			}
		}
		
	}
	$counter = $counter +1
}
Log-Write -LogPath $LogFile -LineValue "Writing new data file to $outfile"
Try
{
$data | Export-Csv -Path $outfile -NoTypeInformation -Force
#$data | ConvertTo-Csv -NoTypeInformation -Delimiter "`t"| % {$_.Replace('"','')} | Out-File $outfile
}
Catch
{
Log-Error -ErrorDesc "ERROR: Writing File $outfile" -LogPath $LogFile -ExitGracefully $false
Log-Error -ErrorDesc $_.Exception.Message -LogPath $LogFile -ExitGracefully $false
}
Try
{
Copy-Item $outfile "C:\Data\ADUsers1.csv" -Force
}
Catch 
{
Log-Error -ErrorDesc "ERROR: Copying file $outfile" -LogPath $LogFile -ExitGracefully $false
Log-Error -ErrorDesc $_.Exception.Message -LogPath $LogFile -ExitGracefully $false
}

Log-Finish -LogPath $LogFile -StartTime $startDate