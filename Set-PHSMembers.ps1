﻿################################################
#
# Author:		Kirk Kenton
# Version:		1.0
# Description:	Adds users from trusted domains
# to stjoe.org security groups from text file
# written to from NetIQ.
#
################################################


. "C:\scripts\Functions\kirksutils.ps1"
. "C:\scripts\Functions\logging.ps1"
$DebugPreference = "Continue"
$start = Get-Date
$logPath = "c:\scripts\prod\Providence\PHSMembersLog.log"
Log-Start -LogPath $logPath -ScriptName "PHS Members" -StartTime $start
$myerror = $false

# Test file path#>
# $dataFilePath = "c:\scripts\prod\Providence"
# Production file path
$dataFilePath = "\\NIMAPP004-VP\phsmembers"
# Data file name filter
$fileNameFilter = "phsmembers*.csv"
# CSV File headers
$csvHeaders = "domain","username","groups"

# Get data files list from path
try {
	$dataFiles = Get-ChildItem $dataFilePath -Filter $fileNameFilter -Name
} catch {
	Log-Error -ErrorDesc "Error retrieving data files." -LogPath $logPath -ExitGracefully $false
	Log-Error -ErrorDesc $_.Exception.Message -LogPath $logPath -ExitGracefully $false
	$myerror = $true
}

# import AD Powershell module
Import-Module activedirectory

# Import data file
# Data file line format is "domain","username","Group1|Group2|Group3"
If ($dataFiles -ne $null) {
	Try {
		# Import and process each data file.
		foreach ($file in $dataFiles) {
			Try{ 
				$data = import-csv -Path "$dataFilePath\$file" -Header $csvHeaders
				Log-Write -LineValue "Processing file: $file" -LogPath $logPath
				} Catch {
					Log-Error -ErrorDesc "Error importing data from $file" -LogPath $logPath -ExitGracefully $false
					Log-Error -ErrorDesc $_.Exception.Message -LogPath $logPath -ExitGracefully $false
					$myerror = $true
				}
			
			#loop through each line in the file
			foreach ($line in $data) {
				Try {
					$FSPUser = $null
					# check that domain is a providence.org domain name
					if($line.domain -like "*.providence.org") {
					# retrieve the user object to add to the group
					$FSPUser = Get-ADUser -identity $line.username -Server $line.domain
					}
				} Catch {
					$username = $line.username
					$domain = $line.domain
					$exception = $_.Exception.Message
					Log-Error -ErrorDesc "$domain\${username}: $exception" -LogPath $logPath -ExitGracefully $false
#					Log-Error -ErrorDesc $_.Exception.Message -LogPath $logPath -ExitGracefully $false
					$myerror = $true
				}
				# Loop through each group listed in the data file.
				if($FSPUser -ne $null) {
					foreach ($group in $line.groups.split("|")){
						Try {
							# Add the providence user to the group
							Add-ADGroupMember -Identity $group -Members $FSPUser -Server ad-dc-003-vp.stjoe.org
							Log-Write -LineValue "$FSPUser|$group" -LogPath $logPath
						} Catch {
							$username = $line.username
							$exception = $_.Exception.Message
							Log-Error -ErrorDesc "adding user ${username} to ${group}: ${exception}" -LogPath $logPath -ExitGracefully $false
#							Log-Error -ErrorDesc $_.Exception.Message -LogPath $logPath -ExitGracefully $false
							$myerror = $true
						}
					}
				}
			}
			# Rename Data file to append .done
			Rename-Item -Path "$dataFilePath\$file" -NewName "$file.done"
		}
	} Catch {
			$exception = $_.Exception.Message
			Log-Error -ErrorDesc "Something went wrong!: ${exception}" -LogPath $logPath -ExitGracefully $false
#			Log-Error -ErrorDesc $_.Exception.Message -LogPath $logPath -ExitGracefully $false
			$myerror = $true
	}
} else {
	Log-Write -LineValue "Warning: No data files found for processing." -LogPath $logPath
	$myerror = $true
}

# add closing footer with elapsed run time to log file and close.
Log-Finish -LogPath $logPath -StartTime $start

if ($myerror) {
	Try {
		Send-MailMessage -SmtpServer "smtp-relay.stjoe.org" -To "kirk.kenton@stjoe.org" -Subject "PHS Members Script Error" -Body "The PHS Member Script has completed with errors. Please review the log for errors and correct as appropriate." -BodyAsHtml -From "ScriptsOnNIMAPP004-VP@stjoe.org" -Attachments $LogPath -ErrorAction Inquire -ErrorVariable kirk
		Exit 1
	} Catch {
		Log-Error -ErrorDesc $_.Exception.Message -LogPath $logPath
	}
} Else {
	Try {
		Send-MailMessage -SmtpServer "smtp-relay.stjoe.org" -To "kirk.kenton@stjoe.org" -Subject "PHS Members Script Error" -Body "The PHS Member Script has completed with errors. Please review the log for errors and correct as appropriate." -BodyAsHtml -From "ScriptsOnNIMAPP004-VP@stjoe.org" -Attachments $LogPath
		Exit 0
	} Catch {
		Log-Error -ErrorDesc $_.Exception.Message -LogPath $logPath
	}
}


